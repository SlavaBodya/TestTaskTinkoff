package res;


public enum ErrorMessages {
    IS_NOT_PRESENT(" is not present"),
    INCORRECT_CONF("Incorrect configuration file settings, current driver type is: "),
    SEARCH_MODULE("The search module is missing"),
    INCORRECT_DRIVER_TYPE("Incorrect driver type: ");

    ;

    private String message;

    ErrorMessages(String message) {
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

}
