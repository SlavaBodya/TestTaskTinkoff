package driverApi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static driverApi.BrowserFactory.getDriver;
import static helpers.PropertiesReader.getExplicitWaitTimeout;

public abstract class DriverApi {

    private WebDriver driver = getDriver();
    protected WebDriverWait wait = new WebDriverWait(driver, getExplicitWaitTimeout());

    public DriverApi refresh() {
        driver.navigate().refresh();
        return this;
    }

    /**
     * @return string value current url
     */
    public final String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * get to url
     * @param url
     * @return this
     */
    protected final DriverApi get(String url) {
        driver.get(url);
        return this;
    }

    /**
     * @param locator
     * @return WebElement
     */
    protected final WebElement findElement(By locator){
        return driver.findElement(locator);
    }

    /**
     * @param locator
     * @return List<WebElement>
     */
    protected final List<WebElement> findElements(By locator) {
        return driver.findElements(locator);
    }

    /**
     * @param locator
     * @param value
     * @return string value attribute
     */
    protected final String getAttribute(By locator,String value){
        return findElement(locator).getAttribute(value);
    }

    /**
     * enter data
     * @param locator
     * @param values
     */
    protected final void enter(By locator, String values) {
//        waitForElement(locator);
        findElement(locator).clear();
        findElement(locator).sendKeys(values);
    }

    /** wait for element located
     * @param locator
     */
    protected final void waitForElement(By locator){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    /**
     * @param locator
     * @return true if element found and displayed
     */
    protected final boolean isSingleElementPresent(By locator) {
        return findElements(locator).size() != 0 && findElements(locator).get(0).isDisplayed();
    }

    /**
     * wait for elemt is preset end click on it
     * @param locator
     */
    protected final void click(By locator){
        waitForElement(locator);
        moveToElement(locator);
        findElement(locator).click();
    }

    /**
     * @param locator
     * @return string text value
     */
    protected final String getText(By locator){
        return findElement(locator).getText();
    }

    /**
     * wait for element move to it
     * @param locator
     */
    private void moveToElement(By locator){
        waitForElement(locator);
        new Actions(driver).moveToElement(findElement(locator));
    }
}
