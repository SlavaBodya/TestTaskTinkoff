package driverApi;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.net.MalformedURLException;
import java.net.URL;

import static helpers.DriverType.LOCAL;
import static helpers.DriverType.REMOTE;
import static helpers.PropertiesReader.*;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static res.ErrorMessages.INCORRECT_CONF;
import static ru.stqa.selenium.factory.WebDriverPool.DEFAULT;

public enum BrowserFactory {


    /**
     * depending on the conf.properties given by the local or remote driver
     */
    CHROME("chrome") {
        public WebDriver create(){
            setupDriver();
            if (currentDriverType.equals(LOCAL)){
                Capabilities chromeDriver = new ChromeOptions();
                return  DEFAULT.getDriver(chromeDriver);
            }
            else if (currentDriverType.equals(REMOTE)){
                try {
                    return  DEFAULT.getDriver(new URL(getRemoteDriverUrl()),
                            new ChromeOptions());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
            throw new IllegalArgumentException(INCORRECT_CONF.getMessage()
                    +currentDriverType);
        }
    };

    public WebDriver create(){
        return CHROME.create();
    }

    private String  driverType;

    BrowserFactory(String driverType) {
        this.driverType = driverType;

    }

    public String getDriverType() {
        return driverType;
    }

    /**
     * @return WebDriver
     */
    public static WebDriver getDriver() {
        if (currentBrowserType.equals(BrowserFactory.CHROME))
            return BrowserFactory.CHROME.create();
        throw new IllegalArgumentException(INCORRECT_CONF.getMessage()
                +currentBrowserType);
    }

    /**
     * check os and download driver
     */
    private static void setupDriver(){
        chromedriver().setup();
    }

}
