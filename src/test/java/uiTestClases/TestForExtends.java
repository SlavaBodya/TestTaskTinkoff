package uiTestClases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import ru.stqa.selenium.factory.WebDriverPool;

import java.util.concurrent.TimeUnit;

import static driverApi.BrowserFactory.getDriver;
import static helpers.PropertiesReader.*;

public class TestForExtends {

    protected WebDriver driver;
    protected WebDriverWait wait = new WebDriverWait(getDriver(), getExplicitWaitTimeout());

    public TestForExtends() {
    }

    @BeforeMethod()
    public void testPreconditions(){
        startDriver();
    }

    @AfterSuite
    public void testPostConditions( ){
        stopDriver();
    }

    /**
     * @return url current page in string format
     */
    protected String checkCurrentUrl() {
        return driver.getCurrentUrl();
    }

    private void setTimeout(){
        driver.manage().timeouts().pageLoadTimeout(getPageLoadTimeout(), TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(getImplicitlyWaitTimeout(), TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(getScriptTimeout(),TimeUnit.SECONDS);
    }

    /**
     * method for stopping current driver
     */
    protected void stopDriver(){
        if (driver != null)
            WebDriverPool.DEFAULT.dismissDriver(driver);
    }

    /**
     * method setup timeouts, screen size and stars the driver
     */
    protected void startDriver(){
        driver = getDriver();
        setTimeout();
        getScreenSize(driver);
    }

    protected void waitForSpinerEnded() {
        By spiner = By.xpath("//*[contains(@class,'LoaderRound') and @data-qa-node='svg']");
        wait.until(ExpectedConditions.invisibilityOfElementLocated(spiner));
    }

}
