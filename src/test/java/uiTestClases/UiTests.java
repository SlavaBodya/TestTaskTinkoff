package uiTestClases;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.ActionOnGkhPage;
import pages.MainPage;
import pages.PaymentsPage;
import pages.gkhPage.GKHPage;
import pages.gkhPage.models.ItemGKH;
import pages.modules.models.SearchItem;

import java.util.List;

import static org.testng.Assert.*;

public class UiTests extends TestForExtends {

    private MainPage mainPage;
    private PaymentsPage paymentsPage;
    private GKHPage gkhPage;
    private ItemGKH itemGKH;
    private ActionOnGkhPage actionOnGkhPage;

    private SoftAssert softAssert = new SoftAssert();

    @Override
    @BeforeMethod
    public void testPreconditions() {
        if (driver == null) {
            super.testPreconditions();
            mainPage = new MainPage();
            paymentsPage =  new PaymentsPage();
            gkhPage = new GKHPage();
        }
    }

    /**
     * 1. Переходом по адресу https://www.tinkoff.ru/ загрузить стартовую страницу Tinkoff Bank.
     2. Из верхнего меню, нажатием на пункт меню “Платежи“, перейти на страницу “Платежи“.
     3. В списке категорий платежей, нажатием на пункт “Коммунальные платежи“, перейти на страницу выбора поставщиков услуг.
     4. Убедиться, что текущий регион – “г. Москва” (в противном случае выбрать регион “г. Москва” из списка регионов).
     5. Со страницы выбора поставщиков услуг, выбрать 1-ый из списка (Должен быть “ЖКУ-Москва”).
     Сохранить его наименование (далее “искомый”) и нажатием на соответствующий элемент перейти на страницу оплаты “ЖКУ-Москва“.
     6. На странице оплаты, перейти на вкладку “Оплатить ЖКУ в Москве“.
     7. Выполнить проверки на невалидные значения для обязательных полей:
     проверить все текстовые сообщения об ошибке (и их содержимое), которые появляются под соответствующим полем ввода
     в результате ввода некорректных данных.
     */
    @Test(priority = 1)
    public void checkingValidationOfMandatoryFieldsOnThePaymentPage() {
        mainPage.open()
                .clickTabItemSecondMenu("Платежи");
        paymentsPage.clickOnCategoryOfPayment("ЖКХ");
        assertTrue(gkhPage.getCurrentLocation().contains("Москве"));
        List<ItemGKH> itemGKHList = gkhPage.getListGKHItems();
        assertEquals(itemGKHList.get(0).getTextTitle(),"ЖКУ-Москва");
        itemGKH = itemGKHList.get(0);
        gkhPage.clickGkhItem(itemGKH);
        actionOnGkhPage = new ActionOnGkhPage(itemGKH);
        /**
         * Тут можно было заюзать data provider но время прогона увиличиться
         */
        actionOnGkhPage.clickPayTab()
                .submitPaymentForm();
        softAssert.assertEquals(actionOnGkhPage.getPaymentAmountValidateMassage(),"Поле обязательное");
        softAssert.assertEquals(actionOnGkhPage.getPaymentCodeValidateMassage(),"Поле обязательное");
        softAssert.assertEquals(actionOnGkhPage.getPaymentPeriodValidateMassage(),"Поле обязательное");
        softAssert.assertAll();
        actionOnGkhPage
                .enterDataIntoPayTab("1","1","1")
                .submitPaymentForm();
        softAssert.assertEquals(actionOnGkhPage.getPaymentAmountValidateMassage(),"Минимум — 10 \u20BD");
        softAssert.assertEquals(actionOnGkhPage.getPaymentCodeValidateMassage(),"Поле неправильно заполнено");
        softAssert.assertEquals(actionOnGkhPage.getPaymentPeriodValidateMassage(),"Поле заполнено некорректно");
        softAssert.assertAll();
        actionOnGkhPage
                .enterDataIntoPayTab("111111111","000000","15001")
                .submitPaymentForm();
        softAssert.assertEquals(actionOnGkhPage.getPaymentAmountValidateMassage(),"Максимум — 15 000 \u20BD");
        softAssert.assertEquals(actionOnGkhPage.getPaymentCodeValidateMassage(),"Поле неправильно заполнено");
        softAssert.assertEquals(actionOnGkhPage.getPaymentPeriodValidateMassage(),"Поле заполнено некорректно");
        softAssert.assertAll();
    }

    /**
     1. Из верхнего меню, нажатием на пункт меню “Платежи“, перейти на страницу “Платежи“.
     2. В списке категорий платежей, нажатием на пункт “Коммунальные платежи“,
     перейти на страницу выбора поставщиков услуг.
     3. Убедиться, что в списке предложенных провайдеров искомый поставщик первый.
     4. Нажатием на элемент, соответствующий искомому, перейти на страницу “Оплатить ЖКУ в Москве“.
     Убедиться, что загруженная страница та же, что и страница, загруженная в результате шага (5).
     */
    @Test(priority = 2,dependsOnMethods = {"checkingValidationOfMandatoryFieldsOnThePaymentPage"})
    public void checkingTheOrderOfGKHMoscowInTheList() {
        SearchItem searchItem = paymentsPage.open()
                .getSearchModule()
                .enterData(itemGKH.getTextTitle())
                .getListSearchedItem()
                .get(0);
        assertEquals(searchItem.getName(),itemGKH.getTextTitle());
        paymentsPage.getSearchModule()
                .clickBySearchedItem(searchItem);
        try {
            wait.until(ExpectedConditions.urlMatches(itemGKH.getTextHref()));
        } catch (TimeoutException e) {
            throw new AssertionError("current page is: "+checkCurrentUrl()+ "but wait : "+itemGKH.getTextHref());
        }
    }

    /**
     *
     1 . Из верхнего меню, нажатием на пункт меню “Платежи“, перейти на страницу “Платежи“.
     2. В списке категорий платежей, нажатием на пункт “Коммунальные платежи“, перейти на страницу выбора поставщиков услуг.
     3. В списке регионов выбрать “г. Санкт-Петербург”.
     4. Убедится, что в списке поставщиков на странице выбора поставщиков услуг отсутствует искомый.
     */
    @Test(priority = 3,dependsOnMethods = {"checkingValidationOfMandatoryFieldsOnThePaymentPage"})
    public void isItemInAnotherLocation() {
        gkhPage.open();
        gkhPage.clickLocation()
                .selectCity("Санкт-Петербург");
        waitForSpinerEnded();
        assertFalse(gkhPage.getListGKHItems().contains(itemGKH),itemGKH.getTextHref()+" item is on the page");
    }

}
