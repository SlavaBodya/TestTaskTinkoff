package helpers;

import driverApi.BrowserFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static helpers.ConstForPropertiesReader.*;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;

public class PropertiesReader {

    private static Properties properties = new Properties();
    public static DriverType currentDriverType = getDriverType();
    public static BrowserFactory currentBrowserType = getBrowserType();

    /**
     * This method allows you to read values from the config file if the value is missing or commentary
     * returns the default value
     * @param key
     * @param defaultValue
     * @return string from the configuration file by the assigned key
     */
    private static String propertiesReader(String key, String defaultValue){
        try{
            FileInputStream fileInputStream = new FileInputStream(CONF_PROPERTIES_PATH);
            properties.load(fileInputStream);
        } catch (IOException e){
            throw new IllegalArgumentException("ERROR: No properties file!");
        }
        if ( properties.getProperty(key) != null &&  !properties.getProperty(key).equals("")){
            return properties.getProperty(key);
        } else return defaultValue;

    }

    /**
     * Returns the type of the core based on the configuration file or the default value
     * @return DriverType
     */
    private static DriverType getDriverType(){
        if (propertiesReader(KEY_DRIVER_TYPE,DEFAULT_DRIVER_TYPE).equals(DriverType.LOCAL.getType())) {
            return DriverType.LOCAL;
        }
        else if (propertiesReader(KEY_DRIVER_TYPE,DEFAULT_DRIVER_TYPE).equals(DriverType.REMOTE.getType())) {
            return DriverType.REMOTE;
        }
        throw new IllegalArgumentException("Incorrect configuration file settings");
    }

    /**
     * The method makes up the url for the remote driver
     * @return remote driver url
     */
    public static String getRemoteDriverUrl(){
        return "http://"+propertiesReader(KEY_HUB_IP,DEFAULT_REMOTE_DRIVER_IP)+":"
                + propertiesReader(KEY_HUB_PORT,DEFAULT_REMORE_DRIVER_PORT)+"/wd/hub";
    }

    /**
     * @return Returns the type of the browser, depending on the settings of the configuration file,
     * if the field is missing or both fields are commented out, returns chromium
     * @throws IllegalArgumentException
     */
    private static BrowserFactory getBrowserType(){
        if (propertiesReader(KEY_BROWSER_TYPE,DEFAULT_BROWSER_TYPE).equals(BrowserFactory.CHROME.getDriverType())){
            return BrowserFactory.CHROME;
        }
        throw new IllegalArgumentException("Incorrect configuration file settings");
    }

    /**
     * Method read params with conf file and sets the screen size to the same settings
     * @param driver
     */
    public static void getScreenSize(WebDriver driver){
        boolean isFullscreen = parseBoolean(propertiesReader(KEY_FULLSCREEN,DEFAULT_FULLSCREEN));
        int width = parseInt(propertiesReader(KEY_WIDTH_BROWSER_WINDOW,DEFAULT_WIDTH_BROWSER_WINDOW));
        int height = parseInt(propertiesReader(KEY_HEIGHT_BROWSER_WINDOW,DEFAULT_HEIGHT_BROWSER_WINDOW));
        if (isFullscreen){
            driver.manage().window().maximize();
        }
        else if ( width > 0 && height > 0  ){
            driver.manage().window().setSize(new Dimension(width,height));
        }
        else throw new IllegalArgumentException("Incorrect configuration file settings");
    }

    /**
     * When adding new sources of work with the email method should be rewritten
     * @return mailSourse
     */

    public static int getImplicitlyWaitTimeout(){
        return parseInt(propertiesReader(KEY_IMPLICITLY_WAIT_TIMEOUT, DEFAULT_IMPLICITLY_WAIT_TIMEOUT));
    }

    public static int getPageLoadTimeout(){
        return parseInt(propertiesReader(KEY_PAGE_LOAD_TIMEOUT,DEFAULT_PAGE_LOAD_TIMEOUT));
    }

    public static int getScriptTimeout(){
        return parseInt(propertiesReader(KEY_SCRIPT_TIMEOUT,DEFAULT_SCRIPT_TIMEOUT));
    }

    public static int getExplicitWaitTimeout(){
        return parseInt(propertiesReader(KEY_EXPLICIT_WAIT_TIMEOUT, DEFAULT_EXPLICIT_WAIT_TIMEOUT));
    }
}

