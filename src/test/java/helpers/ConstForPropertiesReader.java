package helpers;


import static driverApi.BrowserFactory.CHROME;


public class ConstForPropertiesReader {

    /**
     * For locales methods
     */
    static final String CONF_PROPERTIES_PATH = "conf.properties";
    /**
     * For driver type,remote driver ip and port
     */
    static final String KEY_DRIVER_TYPE = "DRIVER_TYPE";
    static final String DEFAULT_DRIVER_TYPE = "local_driver";
    static final String KEY_HUB_IP = "HUB_IP";
    static final String KEY_HUB_PORT = "HUB_PORT";
    static final String DEFAULT_REMOTE_DRIVER_IP = "localhost";
    static final String DEFAULT_REMORE_DRIVER_PORT = "4444";
    /**
     * For browser type
     */
    static final String KEY_BROWSER_TYPE = "BROWSER_TYPE";
    static final String DEFAULT_BROWSER_TYPE = CHROME.getDriverType();
    /**
     * For screen setting
     */
    static final String KEY_WIDTH_BROWSER_WINDOW = "WIDTH_BROWSER_WINDOW";
    static final String KEY_HEIGHT_BROWSER_WINDOW = "HEIGHT_BROWSER_WINDOW";
    static final String KEY_FULLSCREEN = "FULLSCREEN";
    static final String DEFAULT_WIDTH_BROWSER_WINDOW = "1920";
    static final String DEFAULT_HEIGHT_BROWSER_WINDOW = "1080";
    static final String DEFAULT_FULLSCREEN = "false";
    /**
     * For webDriver timeouts
     */
    static final String KEY_IMPLICITLY_WAIT_TIMEOUT = "IMPLICITLY_WAIT_TIMEOUT";
    static final String KEY_PAGE_LOAD_TIMEOUT = "PAGE_LOAD_TIMEOUT";
    static final String KEY_SCRIPT_TIMEOUT = "SCRIPT_TIMEOUT";
    static final String KEY_EXPLICIT_WAIT_TIMEOUT = "EXPLICIT_WAIT_TIMEOUT";
    static final String DEFAULT_IMPLICITLY_WAIT_TIMEOUT = "3";
    static final String DEFAULT_PAGE_LOAD_TIMEOUT = "60";
    static final String DEFAULT_SCRIPT_TIMEOUT = "60";
    static final String DEFAULT_EXPLICIT_WAIT_TIMEOUT ="60";
}
