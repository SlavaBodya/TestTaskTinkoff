package helpers;

import org.openqa.selenium.By;

public class Locators {

    public static By xpath(String locator){
        return By.xpath(locator);
    }

    public static By css(String locator) {
        return By.cssSelector(locator);
    }

    public static By id(String locator) {
        return By.id(locator);
    }

    public static By getNewXpath(By locator, String path) {
        return xpath(locator.toString().replaceAll("By.xpath: ","")+path);
    }
}
