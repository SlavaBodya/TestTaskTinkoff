package helpers;


public enum DriverType {
    LOCAL("local_driver"),
    REMOTE("remote_driver");

    private String type;

    DriverType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
