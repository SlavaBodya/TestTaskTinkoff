package pages;

import org.openqa.selenium.By;

import static helpers.Locators.xpath;

public class MainPage extends PageForExtends {

    /**
     * open main page
     * @return this
     */
    public MainPage open() {
        openPage("");
        return this;
    }

    /**
     * click on tag item by name
     * @param itemName
     * @return this
     */
    public MainPage clickTabItemSecondMenu(String itemName) {
        By TAB_ITEM_SECOND_MENU = xpath("//span[@data-qa-file='SecondMenu' and contains(.,'"+itemName+"') ]");
        click(TAB_ITEM_SECOND_MENU);
        return this;
    }
}
