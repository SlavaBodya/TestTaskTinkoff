package pages;

import org.openqa.selenium.By;

import static helpers.Locators.xpath;

public class PaymentsPage extends PageForExtends {

    private static final String PATH = "payments/";

    /**
     * open payments page
     * @return this
     */
    public PaymentsPage open() {
        openPage(PATH);
        return this;
    }

    /**
     * click on tab item categori of payment by name
     * @param categoryName
     * @return this
     */
    public PaymentsPage clickOnCategoryOfPayment(String categoryName) {
        By categoryItem = xpath("//a[@title='"+categoryName+"']/..");
        click(categoryItem);
        return this;
    }
}
