package pages.gkhPage.models;

import java.util.Objects;

public class ItemGKH {

    private final String imgHref;
    private final String imgTitle;
    private final String textHref;
    private final String textTitle;

    public ItemGKH(String imgHref, String imgTitle, String textHref, String textTitle) {
        this.imgHref = imgHref;
        this.imgTitle = imgTitle;
        this.textHref = textHref;
        this.textTitle = textTitle;
    }

    public String getImgHref() {
        return imgHref;
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public String getTextHref() {
        return textHref;
    }

    public String getTextTitle() {
        return textTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemGKH itemGKH = (ItemGKH) o;
        return Objects.equals(imgHref, itemGKH.imgHref) &&
                Objects.equals(imgTitle, itemGKH.imgTitle) &&
                Objects.equals(textHref, itemGKH.textHref) &&
                Objects.equals(textTitle, itemGKH.textTitle);
    }

    @Override
    public int hashCode() {

        return Objects.hash(imgHref, imgTitle, textHref, textTitle);
    }

    @Override
    public String toString() {
        return "ItemGKH{" +
                "imgHref='" + imgHref + '\'' +
                ", imgTitle='" + imgTitle + '\'' +
                ", textHref='" + textHref + '\'' +
                ", textTitle='" + textTitle + '\'' +
                '}';
    }
}
