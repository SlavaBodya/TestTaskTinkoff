package pages.gkhPage;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.PageForExtends;
import pages.gkhPage.models.ItemGKH;
import pages.modules.LocationModule;

import java.util.LinkedList;
import java.util.List;

import static helpers.Locators.*;

public class GKHPage extends PageForExtends {

    private static final String PATH = "payments/categories/kommunalnie-platezhi/";

    private final By ITEMS_GKH = xpath("//li[@data-qa-file='UIMenuItemProvider']");
    private final By LOCATION = xpath("//span[@data-qa-file='PaymentsCatalogHeader']/span[@data-qa-file='Link']");

    /**
     * open gkh page
     * @return this
     */
    public GKHPage open() {
        openPage(PATH);
        return this;
    }

    /**
     * @return return text onto link location
     */
    public String getCurrentLocation() {
        return getText(LOCATION);
    }

    /**
     * click on link location
     * @return LocationModule
     */
    public LocationModule clickLocation() {
        click(getNewXpath(LOCATION,"/.."));
        return new LocationModule();
    }

    /**
     * wait for url is changed and crate list gkh items
     * @return List<ItemGKH>
     */
    public List<ItemGKH> getListGKHItems() {
        wait.until(ExpectedConditions.urlMatches(getUrl()+PATH));
        waitForElement(ITEMS_GKH);
        int size = findElements(ITEMS_GKH).size();
        List<ItemGKH> itemGKHList = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            int j = i+1;
            String imgHref = getAttribute((getNewXpath(ITEMS_GKH,"["+j+"]/span[1]/a")),"href");
            String imgTitle = getAttribute((getNewXpath(ITEMS_GKH,"["+j+"]/span[1]/a")),"title");
            String textHref = getAttribute((getNewXpath(ITEMS_GKH,"["+j+"]/span[2]/a")),"href");
            String textTitle = getAttribute((getNewXpath(ITEMS_GKH,"["+j+"]/span[2]/a")),"title");
            itemGKHList.add(new ItemGKH(imgHref,imgTitle,textHref,textTitle));
        }
        return itemGKHList;
    }

    /**
     * click on item
     * @param itemGKH
     * @return this
     */
    public GKHPage clickGkhItem(ItemGKH itemGKH) {
        By itemLocator = xpath("//a[@title='"+itemGKH.getImgTitle()+"']/..");
        click(itemLocator);
        return this;
    }


}
