package pages;

import driverApi.DriverApi;
import pages.modules.SearchModule;

public abstract class PageForExtends extends DriverApi {

    private SearchModule searchModule;
    private static final String URL = "https://www.tinkoff.ru/";

    public abstract PageForExtends open();

    /**
     * @return string main url
     */
    protected String getUrl() {
        return URL;
    }

    /**
     * navigate to url(main url+path)
     * @param path
     */
    protected  void openPage(String path){
        get(URL+path);
    }

    /**
     * @return SearchModule
     */
    public SearchModule getSearchModule() {
        if (searchModule == null) {
            searchModule = new SearchModule();
            return searchModule;
        } else
        return searchModule;
    }

}
