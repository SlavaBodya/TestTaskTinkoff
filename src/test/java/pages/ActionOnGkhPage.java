package pages;

import org.openqa.selenium.By;
import pages.gkhPage.models.ItemGKH;

import static helpers.Locators.*;

public class ActionOnGkhPage extends PageForExtends {

    private final By TAB_PAY = xpath("//span[contains(.,'Оплатить')]");
    private final By INPUT_PAYER_CODE = css("[data-qa-file='Grid'] [id='payerCode']");
    private final By VALIDATION_MESSAGE_INPUT_PAYER_CODE =
            xpath("//div[@data-qa-file='UIInput']/../div[@data-qa-file='UIFormRowError']");
    private final By INPUT_PERIOD = id("period");
    private final By VALIDATION_MESSAGE_INPUT_PERIOD =
            xpath("//div[@data-qa-file='UIInputDate']/../div[@data-qa-file='UIFormRowError']");
    private final By INPUT_PAYMENT_AMOUNT = css("[data-qa-file='FormFieldSet'] input");
    private final By VALIDATION_MESSAGE_PAYMENT_AMOUNT
            = css("[data-qa-file='FormFieldSet'] [data-qa-file='UIFormRowError']");

    private final By BTN_SUBMIT_PAYMENT_FORM = xpath("//button[@data-qa-file='UIButton']");

    private ItemGKH itemGKH;

    public ActionOnGkhPage(ItemGKH itemGKH) {
        this.itemGKH = itemGKH;
    }

    /**
     * open gkh item page
     * @return this
     */
    public ActionOnGkhPage open() {
        openPage(itemGKH.getTextHref());
        return this;
    }

    /**
     * click on tab pay
     * @return this
     */
    public ActionOnGkhPage clickPayTab() {
        click(TAB_PAY);
        return this;
    }

    /**
     * enter data to inputs
     * @param payerCode
     * @param period
     * @param paymentAmount
     * @return this
     */
    public ActionOnGkhPage enterDataIntoPayTab(String payerCode,String period,String paymentAmount) {
        enter((INPUT_PAYMENT_AMOUNT),paymentAmount);
        enter((INPUT_PAYER_CODE),payerCode);
        enter((INPUT_PERIOD),period);
        return this;
    }

    /**
     * @return string validate massage input payment code
     */
    public String getPaymentCodeValidateMassage() {
        if (isSingleElementPresent(VALIDATION_MESSAGE_INPUT_PAYER_CODE))
            return getText(VALIDATION_MESSAGE_INPUT_PAYER_CODE);
        else
            return "Payment Code Validate Massage is not present";
    }


    /**
     * @return string validate massage input payment period
     */
    public String getPaymentPeriodValidateMassage() {
        if (isSingleElementPresent(VALIDATION_MESSAGE_INPUT_PERIOD))
            return getText(VALIDATION_MESSAGE_INPUT_PERIOD);
        else
            return "Payment period Validate Massage is not present";
    }


    /**
     * @return string validate massage input payment amount
     */
    public String getPaymentAmountValidateMassage() {
        if (isSingleElementPresent(VALIDATION_MESSAGE_PAYMENT_AMOUNT))
            return getText(VALIDATION_MESSAGE_PAYMENT_AMOUNT);
        else
            return "Payment amount Validate Massage is not present";
    }

    /**
     * @return click on button
     */
    public ActionOnGkhPage submitPaymentForm() {
        click(BTN_SUBMIT_PAYMENT_FORM);
        return this;
    }

}
