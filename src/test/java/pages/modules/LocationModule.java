package pages.modules;

import driverApi.DriverApi;
import org.openqa.selenium.By;

import static helpers.Locators.xpath;

public class LocationModule extends DriverApi {

    /**
     * click on link item by text value
     * @param name
     * @return this
     */
    public LocationModule selectCity(String name) {
        By locator = xpath("//span[contains(.,'"+name+"')]/a");
        click(locator);
        return this;
    }
}
