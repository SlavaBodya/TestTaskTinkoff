package pages.modules;

import driverApi.DriverApi;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import pages.modules.models.SearchItem;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static helpers.Locators.getNewXpath;
import static helpers.Locators.xpath;
import static res.ErrorMessages.SEARCH_MODULE;

public class SearchModule extends DriverApi {

    private final By SEARCH_INPUT = xpath("//input[@data-qa-file='SearchInput']");
    private final By SEARCH_RESULTS =
            xpath("//div[@data-qa-file='GridColumn' and not(contains(.,'Искать в других регионах'))]");
    private final By SEARCH_RESULTS_NAME = getNewXpath(SEARCH_RESULTS,"/div/div[1]/div");
    private final By SEARCH_RESULTS_DESCRIPTION = getNewXpath(SEARCH_RESULTS,"/div/div[2]/div");

    public SearchModule() {
        try {
            waitForElement(SEARCH_INPUT);
        } catch (TimeoutException e) {
            throw new IllegalStateException(SEARCH_MODULE.getMessage() + " on page: "+getCurrentUrl());
        }
    }

    public SearchModule enterData(String data) {
        enter(SEARCH_INPUT,data);
        return this;
    }

    public List<SearchItem> getListSearchedItem() {
        waitForElement(SEARCH_RESULTS);
        int size = findElements(SEARCH_RESULTS).size();
        List<String> names = findElements(SEARCH_RESULTS_NAME).stream()
                .map(WebElement::getText).collect(Collectors.toList());
        List<String> descriptions = findElements(SEARCH_RESULTS_DESCRIPTION).stream()
                .map(WebElement::getText).collect(Collectors.toList());
        List<SearchItem> searchItems = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            searchItems.add(new SearchItem(names.get(i),descriptions.get(i)));
        }
        return searchItems;
    }

    public SearchModule clickBySearchedItem(SearchItem item) {
        By locator = xpath("//div[@data-qa-file='Text' and contains(.,'"+item.getName()+"')]");
        click(locator);
        return this;
    }





}
